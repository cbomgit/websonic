angular.module('playerMain').component('playerMain', {
    templateUrl: 'player-main/player-main.template.html',
    controller: function playerMainController($http, $sce) {
        
        this.apiUrl = "https://music.cbom.me/rest2/";
        this.requiredParameters = "?u=christian&p=Tunes4dayZ!&v=2.2.0&c=rest&f=json"

        this.nowPlaying = {};
        this.nowPlayingCoverArt = this.apiUrl + "getCoverArt.view" + this.requiredParameters;
        this.artistInfo = "";
        this.playQueue = {};
        var self = this;

        //get currently playing song
        $http.get(this.apiUrl + "getNowPlaying.view" + this.requiredParameters).then(function(response) {
            self.nowPlaying = response.data['madsonic-response']['nowPlaying']['entry'][0];
            self.nowPlayingCoverArt += "&id=" + self.nowPlaying.id;
            $http.get(self.apiUrl + "getArtistInfo.view" + self.requiredParameters + "&id=" + self.nowPlaying.id).then(function(response) {
                self.artistInfo = $sce.trustAsHtml(response.data['madsonic-response']['artistInfo']['biography']);
            })
        })

        

        //get the now playing queue
        $http.get(this.apiUrl + "getPlayQueue.view" + this.requiredParameters).then(function(response) {
            self.playQueue = response.data['madsonic-response']['playQueue']['entry'];
        })

        this.formattedSongDuration = function(durationNum) {
            var duration = durationNum.toString();
            return duration.substring(0, duration.length - 2) + ":" + duration.substring(duration.length - 2, duration.length);
        }
    }
});